#!/bin/bash

if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi

cp -r ./server /opt/bpci_sever
rm -f /etc/systemd/system/boardcast_pc_info.service >> /dev/null

echo -e "[Unit]\n\
Description=boardcast_pc_info\n\
[Service]\n\
TimeoutStartSec=30\n\
ExecStart=/usr/bin/python3 /opt/bpci_sever/server.py" >> boardcast_pc_info.service
echo -e 'ExecStop=/bin/kill $MAINPID
Restart=always
RestartSec=10s
[Install]
WantedBy=multi-user.target
' >> boardcast_pc_info.service

mv boardcast_pc_info.service /etc/systemd/system/boardcast_pc_info.service
