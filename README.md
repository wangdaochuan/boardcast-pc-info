# Boardcast pc info
在远程使用pc或者服务器时，若没有固定ip，则远程登陆会变成一个很麻烦的事情，因为不知道目标ip，这是一个用于在ubuntu上部署的脚本
- 它会自动在局域网上发布自己的ip 
![](./doc/fig1.png)

## server install
```bash
git clone https://gitee.com/wangdaochuan/boardcast-pc-info.git
cd boardcast-pc-info
sudo bash install.sh
```

## client use
```bash
git clone https://gitee.com/wangdaochuan/boardcast-pc-info.git
cd boardcast-pc-info/client
python client.py
```
也可以直接下载[发行版](https://gitee.com/wangdaochuan/boardcast-pc-info)


