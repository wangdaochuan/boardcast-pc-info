import socket
import struct
from cryptography.fernet import Fernet
import json
def decrypt(key, s):
    cipher = Fernet(key)
    return cipher.decrypt(s.encode()).decode()
with open("my_dict.bytes", "rb") as f:
    key = f.read()
MCAST_GRP = '224.1.1.1' # 组播地址
MCAST_PORT = 5007 # 端口号

# 创建 UDP 套接字
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)

# 绑定到组播地址和端口号
sock.bind((MCAST_GRP, MCAST_PORT))

# 加入组播组
mreq = struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

# 接收数据报
while True:
    data, addr = sock.recvfrom(1024)
    data_de = decrypt(key,data.decode())
    print("received message:", data_de)

    data_dict = json.loads(data_de)
    
    for i in data_dict:
        print(i,data_dict[i])



