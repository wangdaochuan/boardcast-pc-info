from cryptography.fernet import Fernet
import pickle

def encrypt(key, s):
    cipher = Fernet(key)
    return cipher.encrypt(s.encode()).decode()

def decrypt(key, s):
    cipher = Fernet(key)
    return cipher.decrypt(s.encode()).decode()

key = Fernet.generate_key()
with open("my_dict.bytes", "wb") as f:
    f.write(key)
print(type(key))
s = "Hello, World!"
encrypted = encrypt(key, s)

with open("my_dict.bytes", "rb") as f:
    key2 = f.read()

decrypted = decrypt(key2, encrypted)
print(encrypted)
print(decrypted)
